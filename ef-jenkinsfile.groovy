pipeline{
    agent any

    parameters{
        string (name:'stackname', defaultValue:'elephant', description:'stackname here') 

    }

    environment{
        stackname ="${params.stackname}"
    }

    stages{

        stage('upload index file to s3 and create stack'){
            steps{
                script{
                    sh'''
                    cd /var/lib/jenkins/workspace/cf-pipeline/
                    aws s3 cp index.html s3://admiral-manjinder
                    aws cloudformation create-stack --region us-east-1 --stack-name ${stackname} --template-body file://ef-cf-template.yml
                    '''
                }
            }
        }

       stage('describe created instance'){
                steps{
                    // wait for instance to get created
                    echo "Will describe the created instance"
                    echo "Waiting for the instance to get created and initialised"
                    sleep(time:5,unit:"MINUTES")
                    script{
                        echo "Please look for PublicIpAddress, further this json output can be sliced to show only PublicIP for better user experience"
                        sh '''
                        aws ec2 describe-instances --filters "Name=tag:Name,Values=admiral-manjinder" --region us-east-1
                        '''
                    }
                }
            }

        stage('Delete Created stack'){
            steps{
                script{
                    timeout(time: 7, unit: 'DAYS') {
                     echo "Proceed to delete stack if reviewed already"
                     input message: "Delete Stack?"
                     }   
                     sh '''
                      aws cloudformation delete-stack --region us-east-1 --stack-name ${stackname}
                     '''
                }
            }
        }
    }

    post{
        success{
            sh "echo success"
        }
        failure{
            sh "echo failure"
        }
    }
}

